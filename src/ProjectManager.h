#pragma once

#include <QListWidget>
#include "Project.h"

namespace quwi
{

    class ProjectManager : public QWidget
    {
        Q_OBJECT

    public:
        ProjectManager(QWidget* parent);
        void init(QListWidget* list);
        void getProjectList(QJsonObject& response);

    signals:
        void updateProject(int id, QByteArray& changes);
        void startEditProject(quwi::Project* proj);
        void downloadLogo(QString& url, CALLBACK_SUCCESS_IMAGE func_ok);

    public slots:
        void projectChanged(quwi::Project* proj);
        void downloadLogoBegin(QString& url, CALLBACK_SUCCESS_IMAGE func_ok);

    private:
        void updateView();

    private:
        using projPtr = std::unique_ptr<ProjectObj>;
        std::list<std::unique_ptr<ProjectObj>> projects_;
        Project* selected_;
        QListWidget* list_;

    };

}