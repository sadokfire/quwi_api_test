#pragma once
#include <QMainWindow>
#include <QDockWidget>
#include <QTableWidget>

#include "api/Handler.h"
#include "ProjectManager.h"
#include "ui/ProjectEditWindow.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QuwiAPIViewer; }
QT_END_NAMESPACE


//#define thisApp()  static_cast<::QuwiAPIViewer*>(::QCoreApplication::instance())

class QuwiAPIViewer : public QMainWindow
{
    Q_OBJECT

public:
    QuwiAPIViewer(QWidget *parent = nullptr);
    ~QuwiAPIViewer();

    enum : int {
        PAGE_LOGIN = 0,
        PAGE_PROJECTS,
        PAGE_PROJECT_EDIT,
    };

public slots:
    void goBack();

private:
    void initPageLogin();
    void initPageProjects();
    void initToolbar();
    void initPageProjectEdit();
    void setPage(int nPage);
    void closeEvent(QCloseEvent* event) override;

private:
    QScopedPointer<Ui::QuwiAPIViewer> ui;
    quwi::ui::ProjectEditWindow* ui_proj_edit_;

    int prev_state_ = 0;
    quwi::api::Handler api_;
    quwi::ProjectManager project_man_;

    CALLBACK_SUCCESS getProjectList_;
};
