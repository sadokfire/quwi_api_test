#include "stdafx.h"

#include <QErrorMessage>
#include <QMessageBox>

#include "api/Handler.h"
#include "api/Callback.h"


using namespace Qt;
using namespace quwi::api;

using namespace helper;
using std::placeholders::_1;
//-----------------------------------------------------------------------------
// quwi::api::helper::Sessions
//-----------------------------------------------------------------------------
Sessions::Sessions(QObject* parent)
    : QObject(parent)
{
}
//-----------------------------------------------------------------------------
Sessions::~Sessions()
{
    stopThreads();
}
//-----------------------------------------------------------------------------
void Sessions::stopThreads()
{
    emit stopAll();
}
//-----------------------------------------------------------------------------
void Sessions::addThread(RequestPtr worker)
{
    if (!worker) {
        WARNING_INFO("Sessions::addThread : worker is empty")
        return;
    }
    
    QThread* thread = new QThread;
    connect(thread, SIGNAL(started()), worker, SLOT(run()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

    connect(worker, SIGNAL(finished()), thread, SLOT(quit()));
    connect(this, SIGNAL(stopAll()), worker, SLOT(stop()));

    connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(this, SIGNAL(stopAll()), worker, SLOT(deleteLater()));
    thread->start();
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// QuwiAPIHelper
//-----------------------------------------------------------------------------
QString Handler::host_ = "https://api.quwi.com/v2/";
//-----------------------------------------------------------------------------
Handler::Handler() : sessions_(this)
{
    call_on_fail_ = std::bind(&Handler::showError, this, _1);
}
//-----------------------------------------------------------------------------
void Handler::login(QString& log, QString& pas)
{
#ifdef FAKE_RESPONSE
    authToken_ = "ZDAxYTJkZTUwOTYwYzA0NWU0ZDYyNjkzMmZmNDI5MWY=";
#endif // FAKE_RESPONSE

    if (!authToken_.isEmpty()) {
        emit s_autorizated();
        return;
    }

    CALLBACK_SUCCESS call_ok = std::bind(&Handler::loginOk, this, _1);
    auto worker = makePost(call_ok, call_on_fail_);
    auto& req = worker->getRequest();

    req.setUrl(host_ + "auth/login");
    req.setRawHeader("Content-Type", "application/json");
    req.setRawHeader("Client-Timezone-Offset", "60");
    req.setRawHeader("Client-Device", "desktop");

    QJsonObject data;
    data["email"] = log;
    data["password"] = pas;
    worker->setBody(std::move(data));

    sessions_.addThread(worker);
}
//-----------------------------------------------------------------------------
void Handler::getProjectList(CALLBACK_SUCCESS onOk)
{
#ifdef FAKE_RESPONSE
    QByteArray temp = "{ \"projects\": [{\"id\":1061,\"name\" : \"My Project\",\"uid\" : \"main\",\"logo_url\" : \"https://api.quwi.com/files/projects/1061-thumb.png?v=1\",\"position\" : 1,\"is_active\" : 1,\"is_owner_watcher\" : 1,\"dta_user_since\" : null,\"users\" : [] },{\"id\":1062,\"name\" : \"Project2\",\"uid\" : \"project2\",\"logo_url\" : null,\"position\" : 2,\"is_active\" : 1,\"is_owner_watcher\" : 1,\"dta_user_since\" : null,\"users\" : [] },{\"id\":1075,\"name\" : \"123\",\"uid\" : \"123\",\"logo_url\" : null,\"position\" : 3,\"is_active\" : 1,\"is_owner_watcher\" : 1,\"dta_user_since\" : null,\"users\" : [] }]}";
    onOk(QJsonDocument::fromJson(temp).object());
    return;
#endif // FAKE_RESPONSE

    auto task = makeGet(onOk, call_on_fail_);
    auto& req = task->getRequest();
    req.setUrl(host_ + "projects-manage/index");
    req.setSslConfiguration(QSslConfiguration::defaultConfiguration());
    req.setRawHeader("Content-Type", "application/json");
    setAuth(req);
    
    sessions_.addThread(task);
}
//-----------------------------------------------------------------------------
void Handler::updateProject(int id, QByteArray& changes)
{
#ifdef FAKE_RESPONSE
    emit goBack();
    return;
#endif
    CALLBACK_SUCCESS call_ok = [this](QJsonObject& obj) {
        emit goBack();
    }; 

    auto worker = makePost(call_ok, call_on_fail_);
    auto& req = worker->getRequest();
    req.setUrl(host_ + "projects-manage/update?id=" + QString::number(id));
    req.setSslConfiguration(QSslConfiguration::defaultConfiguration());
    //req.setRawHeader("Content-Type", "multipart/form-data");  // TODO no works https://documenter.getpostman.com/view/3558300/Szf828YP?version=latest#5c25b101-9d04-425f-8b35-8f8b900ee936
    req.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    setAuth(req);
    worker->setBody(changes);

    sessions_.addThread(worker);
}
//-----------------------------------------------------------------------------
void Handler::loginOk(QJsonObject& obj)
{ 
    LOG_INFO("login success")
    authToken_ = obj["token"].toString().toUtf8().toBase64();           /* to secure */
    userId_ = obj["app_init.user.id"].toString().toUtf8().toBase64();   /* to secure */
    emit s_autorizated();
}
//-----------------------------------------------------------------------------
void Handler::showError(QString& response)
{
    DEBUG_INFO("api request failed: " + response)
    QMessageBox::warning(0, tr("ERROR"), response);
}
//-----------------------------------------------------------------------------
void Handler::logout()
{
    sessions_.stopThreads();
    authToken_.clear();
#ifdef FAKE_RESPONSE
    return;
#endif

    RequestPtr worker = makePost( nullptr, call_on_fail_);
    auto& req = worker->getRequest();
    req.setUrl(host_ + "auth/logout");
    req.setSslConfiguration(QSslConfiguration::defaultConfiguration());
    setAuth(req);  
    sessions_.addThread(worker);
}
//-----------------------------------------------------------------------------
void Handler::setAuth(QNetworkRequest& req)
{
    QByteArray auth("Bearer ");
    auth.append(QByteArray::fromBase64(authToken_.toUtf8()));   /* secure */
    req.setRawHeader("Authorization", auth);
}
//-----------------------------------------------------------------------------
void Handler::downloadLogoBegin(QString& url, CALLBACK_SUCCESS_IMAGE func_ok)
{
#ifdef FAKE_RESPONSE_IMAGE
    QFile file(":/res/ico_sample.png");
    file.open(QFile::ReadOnly);
    QByteArray data = file.readAll();
    func_ok(std::move(data));
    return;
#endif // FAKE_RESPONSE

    auto task = makeImageGet(func_ok, call_on_fail_);
    auto& req = task->getRequest();
    req.setUrl(url);
    req.setSslConfiguration(QSslConfiguration::defaultConfiguration());
    //setAuth(req);
    sessions_.addThread(task);
}
//-----------------------------------------------------------------------------

