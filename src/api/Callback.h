#pragma once

#include <functional>
#include <QJsonObject>
#include <QByteArray>
#include <QString>

using CALLBACK_SUCCESS = std::function<void(QJsonObject&)>;
using CALLBACK_SUCCESS_IMAGE = std::function<void(QByteArray&&)>;
using CALLBACK_FAIL = std::function<void(QString&)>;

//#define FAKE_RESPONSE
//#define FAKE_RESPONSE_IMAGE
//#define WRITE_RESPONSE_TO_FILE
//#define CREATE_FAKE_USERS_IF_EMPTY


