#include "stdafx.h"
#include "api/Request.h"
#include "api/Callback.h"

using namespace Qt;
using namespace quwi::api;

//-----------------------------------------------------------------------------
// Request
//-----------------------------------------------------------------------------
std::atomic<size_t> Request::n_ = std::atomic<size_t>(0);
//-----------------------------------------------------------------------------
Request::Request()
    : manager_(this), n_my_(n_++)
{
    QObject::connect(&manager_, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished()), Qt::DirectConnection);
}
//-----------------------------------------------------------------------------
Request::~Request()
{
    //stop();
}
//-----------------------------------------------------------------------------
void Request::replyFinished()
{
    LOG_INFO("request["<< n_my_ <<"] finished" << (reply_->error() == QNetworkReply::NoError ? " with error" : "") << req_.url())
    doFinish();
}
//-----------------------------------------------------------------------------
void Request::doFinish()
{
    if (reply_->error() == QNetworkReply::NoError) {
        QJsonObject obj;
        QByteArray response = reply_->readAll();
#ifdef WRITE_RESPONSE_TO_FILE
        QFile file("Request");
        file.open(QIODevice::WriteOnly);
        file.write(response);
        file.close();
#endif
        QJsonDocument doc = QJsonDocument::fromJson(response);

        // check validity of the document
        if (!doc.isNull()) {
            if (doc.isObject()) {
                obj = doc.object();
            }
            else {
                LOG_INFO("request response is not an object")
            }
        }
        else {
            LOG_INFO("request response is invalid JSON \"" << response <<"\"");
        }
        if (func_ok_) {
            func_ok_(obj);
        }
    }
    else if(func_fail_){
        auto error = "[" + req_.url().toDisplayString() + "]\n" + reply_->errorString();
        func_fail_(std::move(error));
    }
    emit finished();
}
//-----------------------------------------------------------------------------
void Request::run()
{
    LOG_INFO("request[" << n_my_ << "] send" << req_.url())
    doWork();
}
//-----------------------------------------------------------------------------
QNetworkRequest& Request::getRequest()
{
    return req_;
}
//-----------------------------------------------------------------------------
void Request::stop()
{    
    if (reply_) {
        LOG_INFO("request[" << n_my_ << "] stoped" << req_.url())
        reply_->disconnect();
        reply_->deleteLater();
    }
}
//-----------------------------------------------------------------------------
void Request::init(CALLBACK_SUCCESS func_ok, CALLBACK_FAIL func_fail)
{
    func_ok_ = func_ok;
    func_fail_ = func_fail;
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// RequestPost
//-----------------------------------------------------------------------------
void RequestPost::doWork()
{
    reply_ = manager_.post(req_, body_);
}
//-----------------------------------------------------------------------------
void RequestPost::setBody(const QJsonObject& body)
{
    QJsonDocument doc(body);
    body_ = std::move(doc.toJson());
}
//-----------------------------------------------------------------------------
void RequestPost::setBody(const QByteArray& body)
{
    body_ = body;
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// RequestGet
//-----------------------------------------------------------------------------
void RequestGet::doWork()
{
    reply_ = manager_.get(req_);
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// quwi::api::RequestImage
//-----------------------------------------------------------------------------
void RequestImage::init(CALLBACK_SUCCESS_IMAGE func_ok, CALLBACK_FAIL func_fail)
{
    func_ok_image_ = func_ok;
    func_fail_ = func_fail;
}
//-----------------------------------------------------------------------------
void RequestImage::doFinish()
{
    if (reply_->error() == QNetworkReply::NoError) {
        QByteArray response = reply_->readAll();  
#ifdef WRITE_RESPONSE_TO_FILE
        QFile file("RequestImage.png");
        file.open(QIODevice::WriteOnly);
        file.write(response);
        file.close();
#endif
        if (func_ok_image_) {
            func_ok_image_(std::move(response));
        }
    }
    else if (func_fail_) {
        auto error = "[" + req_.url().toDisplayString() + "]\n" + reply_->errorString();
        func_fail_(std::move(error));
        func_ok_image_("");
    }
    emit finished();
}
//-----------------------------------------------------------------------------
