#pragma once

#include "Request.h"


namespace quwi::api
{

	inline namespace helper {

		// https://habr.com/ru/post/150274/
		class Sessions : public QObject
		{
			Q_OBJECT

		public:
			Sessions(QObject* parent);
			~Sessions();

			void addThread(RequestPtr worker);
			void stopThreads();

		signals:
			void stopAll();

		};

	}

	

	class Handler : public QObject
	{
		Q_OBJECT

	public:
		Handler();

		void login(QString& login, QString& pass);
		void getProjectList(CALLBACK_SUCCESS onOk);
		void logout();

	signals :
		void s_autorizated();
		void projectListUpdate(QJsonObject& response);
		void goBack();

	public slots:
		void updateProject(int id, QByteArray& changes);
		void showError(QString& response);
		void downloadLogoBegin(QString& url, CALLBACK_SUCCESS_IMAGE func_ok);

	private:
		void setAuth(QNetworkRequest& req);
		void loginOk(QJsonObject&);

	private:
		static QString host_;
		QString authToken_;
		QString userId_;
		helper::Sessions sessions_;
		CALLBACK_FAIL call_on_fail_;

	};

}