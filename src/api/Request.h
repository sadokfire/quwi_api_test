#pragma once

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>

#include "api/Callback.h"

namespace quwi::api
{
    
    class Request : public QObject
    {
        Q_OBJECT
    public:
        Request();
        virtual ~Request();

        void init(CALLBACK_SUCCESS func_ok, CALLBACK_FAIL func_fail);
        QNetworkRequest& getRequest();
   
    protected:
        virtual void doWork() = 0;
        virtual void doFinish();

    signals:
        void finished();

    protected slots:
        void run();
        void stop();
        void replyFinished();
   
    protected:
        static std::atomic<size_t> n_;
        size_t n_my_;
        QNetworkRequest req_;
        QNetworkReply* reply_ = nullptr;
        QNetworkAccessManager manager_;
        
        CALLBACK_SUCCESS func_ok_ = nullptr;
        CALLBACK_FAIL func_fail_ = nullptr;

    };


    class RequestPost : public Request
    {
        Q_OBJECT
    public:
        RequestPost() = default;
        virtual ~RequestPost() = default;

        void setBody(const QJsonObject& jsonBody);
        void setBody(const QByteArray& body);

    protected:
        void doWork() override;

    protected:
        QByteArray body_;

    };


    class RequestGet : public Request
    {
        Q_OBJECT
    public:
        RequestGet() = default;
        virtual ~RequestGet() = default;

    protected:
        void doWork() override;

    };


    class RequestImage : public RequestGet
    {
        Q_OBJECT
    public:
        RequestImage() = default;
        virtual ~RequestImage() = default;

        void init(CALLBACK_SUCCESS_IMAGE func_ok, CALLBACK_FAIL func_fail);
        void doFinish() override;

    protected:
        CALLBACK_SUCCESS_IMAGE func_ok_image_ = nullptr;

    };


    using RequestPtr = Request*;

    inline RequestPost* makePost(CALLBACK_SUCCESS func_ok, CALLBACK_FAIL func_fail)
    {
        RequestPost* ptr = new RequestPost();
        ptr->init(func_ok, func_fail);
        return ptr;
    }

    inline RequestPtr makeGet(CALLBACK_SUCCESS func_ok, CALLBACK_FAIL func_fail)
    {
        RequestPtr ptr = new RequestGet();
        ptr->init(func_ok, func_fail);
        return ptr;
    }

    inline RequestPtr makeImageGet(CALLBACK_SUCCESS_IMAGE func_ok, CALLBACK_FAIL func_fail)
    {
        RequestImage* ptr = new RequestImage();
        ptr->init(func_ok, func_fail);
        return ptr;
    }
}