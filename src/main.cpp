﻿#include "stdafx.h"
#include "QuwiAPIViewer.h"

#include <QApplication>

QScopedPointer<QFile>   m_logFile;

void messageHandler(QtMsgType type, const QMessageLogContext& context, const QString& msg);
void openLogFile();

int main(int argc, char* argv[])
{
#ifdef _WIN32
    ShowWindow(GetConsoleWindow(), 
        //*
        SW_HIDE //*/ SW_SHOWDEFAULT//*/
    );
#endif

    openLogFile();
    QApplication a(argc, argv);
    QuwiAPIViewer w;
    w.setWindowIcon(QIcon(":/res/quwi-logo.png"));
    w.show();

    QFile File(":/res/style.qss");
    File.open(QFile::ReadOnly);
    QString StyleSheet = QLatin1String(File.readAll());

    a.setStyleSheet(StyleSheet.arg("#F0F4F7").arg("#e8ebed").arg("gray").arg("white").arg("#395378"));
    LOG_INFO("init styles finished")
    return a.exec();
}

void openLogFile()
{
    QDir dir("logs");
    if (!dir.exists()) {
        dir.mkpath(".");
    }
    QString fileName = dir.absolutePath() + "/log_" + QDateTime::currentDateTime().toString("yyyy_MM_dd__hh_mm_ss") + ".txt";
    m_logFile.reset(new QFile(fileName));
    m_logFile.data()->open(QFile::Append | QFile::Text);
    qInstallMessageHandler(messageHandler);
    LOG_INFO("init logger finished")
}

void messageHandler(QtMsgType type, const QMessageLogContext& context, const QString& msg)
{
    QTextStream out(m_logFile.data());
    out << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz [");
    //out << QThread::currentThreadId() << "][";
    out << context.category << "]: "
        << msg << "\n";
    out.flush();  
}
