#pragma once
#include <QObject>
#include "api/Callback.h"

namespace quwi
{

    class User : public QObject
    {
        Q_OBJECT
	public:
		User(QJsonObject& sett);
		User(const User& other) = default;

		bool init();
		~User() = default;
		void setLogo(QByteArray&& data);

    signals:
        void downloadLogo(QString& url, CALLBACK_SUCCESS_IMAGE func_ok);
		void logoChanged();

	public:
		bool isLogoDownloading = false;
		__int64 id;
		__int64 is_online;
        QString name;
        QString avatar_url;
        QString dta_activity;
		QByteArray logo;
	};

}