#include "stdafx.h"
#include "User.h"

using namespace Qt;
using namespace quwi;
using std::placeholders::_1;

//-----------------------------------------------------------------------------
// quwi::User
//-----------------------------------------------------------------------------
User::User(QJsonObject& sett)
{
    id = sett["id"].toInt();
    name = sett["name"].toString();
    avatar_url = sett["avatar_url"].toString();
    dta_activity = sett["dta_activity"].toString();
    is_online = sett["is_online"].toBool();
}
//-----------------------------------------------------------------------------
bool User::init()
{
    if (logo.isEmpty() && !isLogoDownloading && !avatar_url.isEmpty()) {
        isLogoDownloading = true;
        CALLBACK_SUCCESS_IMAGE callback = std::bind(&quwi::User::setLogo, this, _1);
        QString temp = avatar_url;
        emit downloadLogo(std::move(temp), callback);
    }
    return false;
}
//-----------------------------------------------------------------------------
void User::setLogo(QByteArray&& data)
{
    logo = std::move(data);
    emit logoChanged();
}
//-----------------------------------------------------------------------------