#include "stdafx.h"

#include "ProjectManager.h"
#include "ui/ProjectItem.h"

using namespace Qt;
using namespace quwi;

//-----------------------------------------------------------------------------
// ProjectManager
//-----------------------------------------------------------------------------
ProjectManager::ProjectManager(QWidget* parent)
    : QWidget(parent)
{
}
//-----------------------------------------------------------------------------
void ProjectManager::init(QListWidget* list)
{
    list_ = list;
}
//-----------------------------------------------------------------------------
void ProjectManager::getProjectList(QJsonObject& response)
{
    auto array = response.value("projects");
    auto projs = array.toArray();
       
    std::list<projPtr> temp;
    for (auto& it : projs) {
        projPtr obj = std::make_unique<ProjectObj>(it.toObject());
        temp.emplace_back(std::move(obj));
    }
    
    bool wasChanging = false;

    /* remove old */
    for (auto it = projects_.begin(); it != projects_.end();) {
        auto& it_val = *(*it);
        int id = it_val.getID();
        auto finded = std::find_if(temp.begin(), temp.end(), [id](const projPtr& obj) {
            return id == obj->getID();
            });

        if (finded == temp.end()) { /* was removed */
            projects_.erase(it);
            it--;
        }
        else {
            it++;
        }
    }

    /* update */
    std::vector<int> ids_to_update;
    for (auto it = temp.begin(); it != temp.end(); it++) {
        auto& it_val = *(*it);
        int id = it_val.getID();
        auto finded = std::find_if(projects_.begin(), projects_.end(), [id](const projPtr& obj) {
                return id == obj->getID();
            });
                
        if (finded == projects_.end()) { /* new one */
            projects_.emplace_back(std::move(*it));
            auto & obj = projects_.back();
            connect(obj.get(), &quwi::ProjectObj::downloadLogo, this, &quwi::ProjectManager::downloadLogoBegin, Qt::DirectConnection);
            wasChanging = true;
        }
        else {  // check update
            auto& stored_val = *(*finded);
            auto changes = stored_val.getDiff(it_val.data_);
            stored_val.update(it_val.data_);
            if (!changes.isEmpty()) {
                ids_to_update.push_back(stored_val.data_.projectId);
            }
        }
    }

    if (!ids_to_update.empty()) {
        foreach(QListWidgetItem * item, list_->findItems("*", Qt::MatchWildcard)) {
            quwi::ui::ProjectItem* my_item = static_cast<quwi::ui::ProjectItem*>(list_->itemWidget(item));
            if (my_item && std::find(ids_to_update.begin(), ids_to_update.end(), my_item->getID())!= ids_to_update.end()) {
                my_item->update();
            }
        }
    }


    for (auto& it : projects_) {
        it->init(false);
    }

    //emit changed
    if (wasChanging) {
        updateView();
    }
}
//-----------------------------------------------------------------------------
void ProjectManager::updateView()
{
    foreach(QListWidgetItem * item, list_->findItems("*", Qt::MatchWildcard)) {
        QWidget* widget = list_->itemWidget(item);
        delete widget;
        delete item;
    }

    auto callback = [=](quwi::Project* proj) {
        emit startEditProject(proj);
    };

    int i = 0;
    for (auto& it: projects_) {
        QListWidgetItem* item = new QListWidgetItem;
        quwi::ui::ProjectItem* my_item = new quwi::ui::ProjectItem(this);
        my_item->init(&it->data_);
        item->setSizeHint(my_item->size());
        list_->insertItem(i++, item);
        list_->setItemWidget(item, my_item);
        connect(my_item, &quwi::ui::ProjectItem::projectWasPressed, this, &quwi::ProjectManager::startEditProject);
        connect(it.get(), &quwi::ProjectObj::logoChanged, my_item, &quwi::ui::ProjectItem::logoChanged, Qt::DirectConnection);
    }
}
//-----------------------------------------------------------------------------
void ProjectManager::projectChanged(quwi::Project* proj)
{
    int id = proj->projectId;
    auto finded = std::find_if(projects_.begin(), projects_.end(), [id](const projPtr& obj) {
            return id == obj->getID();
        });

    if (finded == projects_.end()) { /* not founded */
        WARNING_INFO("ProjectManager::projectChanged : appling change to non existing project")
        return;
    }

    auto& current_val = *(*finded);
    auto diff = current_val.getDiff(*proj);
    if (!diff.isEmpty()) {
        //current_val.update(*proj);
        emit updateProject(id, diff);
    }
}
//-----------------------------------------------------------------------------
void ProjectManager::downloadLogoBegin(QString& url, CALLBACK_SUCCESS_IMAGE func_ok)
{
    emit downloadLogo(url, func_ok);
}
//-----------------------------------------------------------------------------