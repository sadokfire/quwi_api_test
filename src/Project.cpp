#include "stdafx.h"

#include <QDateTime>

#include "Project.h"
#include "ui/ProjectItem.h"
#include "api/Callback.h" /* for fake users */

using namespace Qt;
using namespace quwi;

using std::placeholders::_1;


//-----------------------------------------------------------------------------
// Project
//-----------------------------------------------------------------------------
Project::Project(QJsonObject& sett)
{
    name = sett.value("name").toString();
    spent_time_all = QDateTime::fromTime_t(sett.value("spent_time_all").toInt()).toUTC().toString("hh:mm:ss");
    spentTimeMonth = QDateTime::fromTime_t(sett.value("spent_time_month").toInt()).toUTC().toString("hh:mm:ss");
    spentTimeWeek = QDateTime::fromTime_t(sett.value("spent_time_week").toInt()).toUTC().toString("hh:mm:ss");
    projectId = sett.value("id").toInt();
    is_active_ = sett.value("is_active").toInt();
    logo_url = sett.value("logo_url").toString();
    is_owner_watcher_ = sett.value("is_owner_watcher").toInt();

    QJsonArray arrayUsers = sett["users"].toArray();
    for (const auto& it : arrayUsers) {
        std::unique_ptr<quwi::User> ptr = std::make_unique<quwi::User>(it.toObject());
        users_.emplace_back(std::move(ptr));
    }

#ifdef CREATE_FAKE_USERS_IF_EMPTY
    if (users_.empty()) {
        srand((unsigned)time(0));
        int count = 3 + std::rand() % 30;
        QJsonObject fake_user;
        fake_user["id"] = 6660;
        fake_user["avatar_url"] = "https://api.quwi.com/files/projects/1061-thumb.png?v=1";
        fake_user["dta_activity"] = "2018-08-13 08:31:05";

        for (int i = 0; i < count; i++) {
            fake_user["is_online"] = static_cast<bool>(i%3);
            fake_user["name"] = "mock_" + QString::number(6660 + i);
            std::unique_ptr<quwi::User> ptr = std::make_unique<quwi::User>(fake_user);
            users_.emplace_back(std::move(ptr));
        }
        srand(count);
    }
#endif
}
//-----------------------------------------------------------------------------
Project& Project::operator=(const Project& val)
{
    if (&val == this) {
        return *this;
    }

    is_active_ = val.is_active_;
    is_owner_watcher_ = val.is_owner_watcher_;
    isLogoDownloading = val.isLogoDownloading;

    projectId = val.projectId;

    name = val.name;
    logo_url = val.logo_url;
    logo_url = val.logo_url;
    logo = val.logo;

    spent_time_all = val.spent_time_all;
    spentTimeMonth = val.spentTimeMonth;
    spentTimeWeek = val.spentTimeWeek;

    /*for (auto& it : val.users_) { // TODO full copy
        UserPtr temp = it->copy
        users_.emplace_back(it);
    }*/

    return *this;
}
//-----------------------------------------------------------------------------



//-----------------------------------------------------------------------------
// Project
//-----------------------------------------------------------------------------
ProjectObj::ProjectObj(QJsonObject& sett)
    : data_(sett)
{
    for (auto& it : data_.users_) {
        connect(it.get(), SIGNAL(downloadLogo(QString&,CALLBACK_SUCCESS_IMAGE)), this, SLOT(downloadLogoBegin(QString&, CALLBACK_SUCCESS_IMAGE)), Qt::DirectConnection);
    }
}
//-----------------------------------------------------------------------------
void ProjectObj::update(Project& val)
{
    if (data_.projectId != val.projectId) {
        WARNING_INFO("ProjectObj::update : projectId not equal - update skipped")
        return;
    }

    data_.name.swap(val.name);
    //data_.logo_url.swap(val.logo_url);
    if (data_.logo_url != val.logo_url) { /* cause we can downloaded right now */
        data_.logo_url.swap(val.logo_url);
        data_.logo.clear();
    }
    
    data_.spent_time_all.swap(val.spent_time_all);
    data_.spentTimeMonth.swap(val.spentTimeMonth);
    data_.spentTimeWeek.swap(val.spentTimeWeek);

    //data_.users_.swap(val.users_); // no nessesary, cause all connection are on this users
    for (auto& it : val.users_) {
        int id = it->id;
        auto functor = [id](UserPtr& ptr) {
            return id == ptr->id;
        };

        if (std::find_if(data_.users_.begin(), data_.users_.end(), functor) == data_.users_.end()) { /* new user */
            data_.users_.emplace_back(std::move(it));
            data_.users_.back()->init();
        }
    }

    data_.is_active_ = val.is_active_;
    data_.is_owner_watcher_ = val.is_owner_watcher_;
}
//-----------------------------------------------------------------------------
QByteArray ProjectObj::getDiff(const Project& other) const
{
    QByteArray res;

    if (data_.name != other.name) {
        res += "name=";
        res += other.name.toUtf8();
    }

    // TODO others

    return res;
}
//-----------------------------------------------------------------------------
void ProjectObj::downloadLogoBegin(QString& url, CALLBACK_SUCCESS_IMAGE func_ok)
{
    emit downloadLogo(url, func_ok);
}
//-----------------------------------------------------------------------------
void ProjectObj::setLogo(QByteArray&& data)
{
    data_.logo = std::move(data);
    emit logoChanged();
}
//-----------------------------------------------------------------------------
bool ProjectObj::init(bool is_need_child_init)
{
    bool result = true;
    if (data_.logo.isEmpty() && !data_.isLogoDownloading && !data_.logo_url.isEmpty()) {
        data_.isLogoDownloading = true;
        CALLBACK_SUCCESS_IMAGE callback = std::bind(&quwi::ProjectObj::setLogo, this, _1);
        QString temp = data_.logo_url;
        emit downloadLogo(temp, callback);
        result = false;
    }
    if (is_need_child_init) {
        for (auto& it : data_.users_) {
            result |= it->init();
        }
    }
    return result;
}
//-----------------------------------------------------------------------------
