#include "stdafx.h"
#include "QuwiAPIViewer.h"
#include "src/ui_QuwiAPIViewer.h"


using std::placeholders::_1;


QuwiAPIViewer::QuwiAPIViewer(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::QuwiAPIViewer), project_man_(this)
{
	ui->setupUi(this);
	
	initPageLogin();
	initPageProjects();
	initPageProjectEdit();
	initToolbar();
	setPage(PAGE_LOGIN);
}
//-----------------------------------------------------------------------------
QuwiAPIViewer::~QuwiAPIViewer()
{
    api_.logout();
    disconnect();
    hide();
}
//-----------------------------------------------------------------------------
void QuwiAPIViewer::initPageLogin()
{
    ui->page_login_lineedit_email->setPlaceholderText("Email");
    ui->page_login_lineedit_password->setPlaceholderText("Password");

	
	connect(ui->page_login_button_login, &QPushButton::pressed, this, [&]() {
		api_.login(ui->page_login_lineedit_email->text(), ui->page_login_lineedit_password->text());
	});

    connect(&api_, &quwi::api::Handler::s_autorizated, this, [=]() {
			ui->page_login_lineedit_email->clear();				 /* for secure */
			ui->page_login_lineedit_password->clear();		     /* for secure */
			this->setPage(PAGE_PROJECTS);
    });
}
//-----------------------------------------------------------------------------
void QuwiAPIViewer::initPageProjects()
{
	project_man_.init(ui->page_projects_list);

	getProjectList_ = std::bind(&quwi::ProjectManager::getProjectList, &project_man_, _1);

    connect(&project_man_, &quwi::ProjectManager::downloadLogo, &api_, &quwi::api::Handler::downloadLogoBegin, Qt::DirectConnection);
	    
}
//-----------------------------------------------------------------------------
void QuwiAPIViewer::initPageProjectEdit()
{
	ui_proj_edit_ = new quwi::ui::ProjectEditWindow(this);

	QVBoxLayout* vbox_state = new QVBoxLayout;
    vbox_state->addWidget(ui_proj_edit_);
    vbox_state->addStretch(1);
    ui->page_edit_place->setLayout(vbox_state);

    connect(ui_proj_edit_, SIGNAL(wasChanged(quwi::Project*)), &project_man_, SLOT(projectChanged(quwi::Project*)));
    connect(ui_proj_edit_, &quwi::ui::ProjectEditWindow::s_inited, this, [=]() {
        this->setPage(PAGE_PROJECT_EDIT);
        });

	connect(&project_man_, SIGNAL(startEditProject(quwi::Project*)), ui_proj_edit_, SLOT(init(quwi::Project*)));
    connect(&project_man_, &quwi::ProjectManager::updateProject, &api_, &quwi::api::Handler::updateProject);
}
//-----------------------------------------------------------------------------
void QuwiAPIViewer::initToolbar()
{
    ui->toolbar_logo->setPixmap(QPixmap::fromImage(QImage(":/res/quwi-logo.png")));
    ui->toolbar_logo->setScaledContents(true);

    connect(ui->toolbar_logout, &QPushButton::pressed, this, [&]() {
        api_.logout();
        setPage(PAGE_LOGIN);
        });

    connect(ui->toolbar_projects, &QPushButton::pressed, this, [&]() {
        setPage(PAGE_PROJECTS);
        });

    connect(&api_, SIGNAL(goBack()), this, SLOT(goBack()));
}
//-----------------------------------------------------------------------------
void QuwiAPIViewer::goBack()
{
	setPage(prev_state_);
}
//-----------------------------------------------------------------------------
void QuwiAPIViewer::setPage(int nPage)
{
	prev_state_ = ui->stackedWidget->currentIndex();

	ui->stackedWidget->setCurrentIndex(nPage);
	switch (nPage) {
	case PAGE_LOGIN:
		ui->toolbar->hide();
		break;
	case PAGE_PROJECTS:
		ui->toolbar->show();
		api_.getProjectList(getProjectList_);
		break;
	case PAGE_PROJECT_EDIT:
		ui->toolbar->show();
		break;
	default: 
		break;
	}
}
//-----------------------------------------------------------------------------
void QuwiAPIViewer::closeEvent(QCloseEvent* event)
{
	LOG_INFO("application was closed")
    QWidget::closeEvent(event);
}
//-----------------------------------------------------------------------------