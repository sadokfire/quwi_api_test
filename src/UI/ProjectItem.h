#pragma once
#include <QLabel>
#include <QMouseEvent>
#include "Project.h"

namespace Ui {
class ProjectItem;
}

namespace quwi::ui
{
  
    void makeCroppedLogo(QByteArray& img, QString& txt, int scaleW, int scaleH, QLabel* destination);
    void makeCroppedEmptyLogo(QColor& color, QString& txt, int scaleW, int scaleH, QLabel* destination);
    QPixmap& makeHashedCroppedEmptyLogo(QColor& color, int scaleW, int scaleH);

    class ProjectItem : public QWidget
    {
        Q_OBJECT

    public:
        explicit ProjectItem(QWidget* parent = nullptr);
        ~ProjectItem() = default;

        void update();
        int getID() const;

    signals:
        void projectWasPressed(quwi::Project* proj);

    public slots:
        void init(quwi::Project* projectItem);
        void logoChanged();

    protected:
        void mousePressEvent(QMouseEvent* event) override;

    private:
        QScopedPointer<Ui::ProjectItem> ui;
        quwi::Project* p_item_ = nullptr;

    };

}
