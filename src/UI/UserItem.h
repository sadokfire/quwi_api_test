#pragma once
#include "User.h"

namespace Ui {
class UserItem;
}

namespace quwi::ui
{

    class UserItem : public QWidget
    {
        Q_OBJECT

    public:
        explicit UserItem(QWidget* parent = nullptr);
        ~UserItem() = default;
        void init(quwi::User* user);

    public slots:
        void logoChanged();

    private:
        QScopedPointer<Ui::UserItem> ui;
        quwi::User* data_;

    };

}
