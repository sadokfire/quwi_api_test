#include "stdafx.h"
#include "ui/ProjectItem.h"
#include "ui/ui_ProjectItem.h"
#include <QBitmap>
#include <QPainter>

using namespace quwi::ui;


struct EmptyLogoSetting {
    QColor color;
    int w;
    int h;
    bool operator<(const EmptyLogoSetting& b) const;

};
//-----------------------------------------------------------------------------
bool EmptyLogoSetting::operator<(const EmptyLogoSetting& b) const
{
    return  w < b.w ||
        h < b.h ||
        color.hsvHueF() < b.color.hsvHueF();
}
//-----------------------------------------------------------------------------
static std::map<EmptyLogoSetting, QPixmap> empty_logo_hash;

//-----------------------------------------------------------------------------
void quwi::ui::makeCroppedEmptyLogo(QColor& color, QString& txt, int scaleW, int scaleH, QLabel* destination)
{
    QPixmap& pix = makeHashedCroppedEmptyLogo(color, scaleW, scaleH);
    if (!txt.isEmpty()) {
        QPixmap pix2 = pix.copy();
        QPainter painter2(&pix2);
        painter2.setBrush(QColor("white"));
        painter2.setFont(QFont("Arial", scaleH / 3, 2));
        painter2.drawText(0, 0, scaleW, scaleH, Qt::AlignHCenter | Qt::AlignVCenter, txt);
        destination->setPixmap(pix2);
    }
    else {
        destination->setPixmap(pix);
    }
}
//-----------------------------------------------------------------------------
QPixmap& quwi::ui::makeHashedCroppedEmptyLogo(QColor& color, int scaleW, int scaleH)
{
    EmptyLogoSetting hashKey{ color , scaleW , scaleH };
    auto it = empty_logo_hash.find(hashKey);
    if (it == empty_logo_hash.end()) {
        QPixmap pix(scaleW, scaleH);
        pix.fill(color);

        QBitmap map(scaleW, scaleH);
        map.fill(Qt::color0);

        QPainter painter(&map);
        painter.setBrush(Qt::color1);
        painter.drawEllipse(0, 0, scaleW, scaleH);

        pix.setMask(map);
        empty_logo_hash.emplace(hashKey, pix);
    }

    return empty_logo_hash[hashKey];
}
//-----------------------------------------------------------------------------
void quwi::ui::makeCroppedLogo(QByteArray& img, QString& txt, int scaleW, int scaleH, QLabel* destination)
{
    if (img.isEmpty()) {
        makeCroppedEmptyLogo(QColor("cyan"), txt.left(3), scaleW, scaleH, destination); /* like current web version */
        return;
    }
    QPixmap p;
    p.loadFromData(img);
    QPixmap scaled = p.scaled(scaleW, scaleH, Qt::KeepAspectRatio, Qt::SmoothTransformation);

    QBitmap map(scaled.width(), scaled.height());
    map.fill(Qt::color0);

    QPainter painter(&map);
    painter.setBrush(Qt::color1);
    painter.drawEllipse(0, 0, scaled.width(), scaled.height());
    scaled.setMask(map);

    destination->setPixmap(scaled);
    //destination->setScaledContents(1);
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ProjectItem
//-----------------------------------------------------------------------------
ProjectItem::ProjectItem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProjectItem)
{
    ui->setupUi(this);
}
//-----------------------------------------------------------------------------
void ProjectItem::update()
{
    if (p_item_) {
        init(p_item_);
    }
}
//-----------------------------------------------------------------------------
void ProjectItem::init(quwi::Project* projectItem)
{
    p_item_ = projectItem;
    if (!p_item_) {
        WARNING_INFO("ProjectItem::init : item is empty")
        return;
    }
    ui->ProjectItem_name->setText(p_item_->name);
    ui->ProjectItem_state->setText(p_item_->is_active_ == 1 ? "Active" : "Inactive");

    ui->week_value->setText(p_item_->spentTimeWeek);
    ui->month_value->setText(p_item_->spentTimeMonth);
    ui->total_value->setText(p_item_->spent_time_all);

    //if (!p_item_->logo.isEmpty()) {
        logoChanged();
    //}
}
//-----------------------------------------------------------------------------
void ProjectItem::mousePressEvent(QMouseEvent *event){
    Q_UNUSED(event);
    emit projectWasPressed(p_item_);
}
//-----------------------------------------------------------------------------
void ProjectItem::logoChanged()
{
    makeCroppedLogo(p_item_->logo, p_item_->name, 80, 80, ui->ProjectItem_logo);
}
//-----------------------------------------------------------------------------
int ProjectItem::getID() const
{
    if (!p_item_) {
        return -1;
    }
    return p_item_->projectId;
}
//-----------------------------------------------------------------------------

