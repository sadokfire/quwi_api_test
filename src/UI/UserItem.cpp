#include "stdafx.h"
#include "ui/UserItem.h"
#include "ui/ui_UserItem.h"
#include "ui/ProjectItem.h"   /* for makeCroppedLogo */

using namespace quwi::ui;

//-----------------------------------------------------------------------------
// ProjectItem
//-----------------------------------------------------------------------------
UserItem::UserItem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UserItem)
{
    ui->setupUi(this);
}
//-----------------------------------------------------------------------------
void UserItem::init(quwi::User* user)
{
    if (!user) {
        WARNING_INFO("UserItem::init : user is empty")
        return;
    }


    data_ = user;

    ui->name->setText(data_->name);
    ui->state->setCheckState(1 ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
    QPixmap& pix = makeHashedCroppedEmptyLogo(QColor(data_->is_online ? "green" : "red"), 14, 14);
    ui->onlineStatus->setPixmap(pix);

    if (data_->logo.isEmpty()) {
        connect(data_, &quwi::User::logoChanged, this, &quwi::ui::UserItem::logoChanged, Qt::DirectConnection);
        data_->init();
    }
    logoChanged(); /* for empty draw or ico */
}
//-----------------------------------------------------------------------------
void UserItem::logoChanged()
{
    makeCroppedLogo(data_->logo, data_->name, 34, 34, ui->logo);
}
//-----------------------------------------------------------------------------
