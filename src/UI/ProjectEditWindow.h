#pragma once

#include "Project.h"
#include "ui/ToggleButton.h"

namespace Ui {
class ProjectEditWindow;
}

namespace quwi::ui
{

    class ProjectEditWindow : public QWidget
    {
        Q_OBJECT

    public:
        explicit ProjectEditWindow(QWidget* parent = nullptr);
        ~ProjectEditWindow() = default;

    signals:
        void wasChanged(quwi::Project* val);
        void s_inited();

    public slots:
        void init(quwi::Project* val);
        
    private slots:
        void on_ProjectEditWindow_apply_pressed();

    private:
        void updateView();

    private:
        QScopedPointer<Ui::ProjectEditWindow> ui;
        quwi::Project* old_val_;
        quwi::Project temp_val_;
        ToggleButton* sw_state_;
        ToggleButton* sw_watchers_;

    };
  

}
