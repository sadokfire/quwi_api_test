#include "stdafx.h"
#include "ui/ProjectEditWindow.h"
#include "ui/ui_ProjectEditWindow.h"
#include "ui/ProjectItem.h"
#include "ui/UserItem.h"

using namespace quwi::ui;

//-----------------------------------------------------------------------------
// ProjectEditWindow
//-----------------------------------------------------------------------------
ProjectEditWindow::ProjectEditWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProjectEditWindow)
{
    ui->setupUi(this);
  
    sw_state_ = new ToggleButton(10, 8, this);
    sw_watchers_ = new ToggleButton(10, 8, this);

    QVBoxLayout * vbox_state = new QVBoxLayout;
    vbox_state->addWidget(sw_state_);
    vbox_state->addStretch(1);
    ui->watcher_switch_place->setLayout(vbox_state);

    QVBoxLayout* vbox_watchers = new QVBoxLayout;
    vbox_watchers->addWidget(sw_watchers_);
    vbox_watchers->addStretch(1);
    ui->state_switch_place->setLayout(vbox_watchers);

    ui->ProjectEditWindow_edit->setPlaceholderText("Name");

    ui->tableWidget->setColumnCount(4);
}
//-----------------------------------------------------------------------------
void ProjectEditWindow::init(quwi::Project* item) 
{
    if (!item) {
        WARNING_INFO("ProjectEditWindow::init : item is empty")
        return;
    }
    old_val_ = item;
    temp_val_ = *item;

    ui->ProjectEditWindow_edit->setText(temp_val_.name);

    sw_state_->setChecked(item->is_active_);
    sw_watchers_->setChecked(item->is_owner_watcher_);

    makeCroppedLogo(old_val_->logo, old_val_->name, 90, 90, ui->ProjectEditWindow_logo);
    updateView();
}
//-----------------------------------------------------------------------------
void ProjectEditWindow::updateView()
{
    auto table = ui->tableWidget;
    foreach(QTableWidgetItem * item, table->findItems("*", Qt::MatchWildcard)) {
        if (item) {
            table->removeCellWidget(item->row(), item->column());
        }
    }
        
    const int w = table->columnCount();
    const int h = w ? 1 + static_cast<int>(old_val_->users_.size()) / w : 0;
    table->setRowCount(h);

    int i = 0, r, c;
    for (auto& it : old_val_->users_) { /* TODO fix crunch */
        r = i/w;
        c = i%w;
        i++;
        quwi::ui::UserItem* my_item = new quwi::ui::UserItem(this);        
        my_item->init(it.get());
        table->setCellWidget(r,c, my_item);
    }

    update();
    emit s_inited();
}
//-----------------------------------------------------------------------------
void ProjectEditWindow::on_ProjectEditWindow_apply_pressed()
{
    temp_val_.name = ui->ProjectEditWindow_edit->text();

    // TODO other changes

    emit wasChanged(&temp_val_);
}
//-----------------------------------------------------------------------------