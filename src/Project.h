#pragma once
#include "User.h"

namespace quwi
{

    using UserPtr = std::unique_ptr<quwi::User>;

    struct Project {
        /* api */
        bool is_active_ = false;
        bool is_owner_watcher_ = false;

        int projectId = -1;
        QString name;
        QString logo_url;
        QString spent_time_all;
        QString spentTimeMonth;
        QString spentTimeWeek;

        std::list<UserPtr> users_;

        /* self */
        bool isLogoDownloading = false;
        QByteArray logo;


    public:
        Project() = default;
        Project& operator=(const Project& other);
        Project(QJsonObject& sett);

    };


    class ProjectObj : public QObject
    {
        Q_OBJECT
	public:
        ProjectObj(QJsonObject& sett);
		~ProjectObj() = default;

        int getID() const { return data_.projectId;  }
        void update(Project& newValue);
        void setLogo(QByteArray&& data);
        bool init(bool is_need_child_init);
        QByteArray getDiff(const Project& other) const;

    signals:
        void logoChanged();
        void downloadLogo(QString& url, CALLBACK_SUCCESS_IMAGE func_ok);

    public slots:
        void downloadLogoBegin(QString& url, CALLBACK_SUCCESS_IMAGE func_ok);

    public:
        Project data_;

	};
}