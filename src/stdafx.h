#ifndef MT_stdafx_h
#define MT_stdafx_h

#define __STDC_FORMAT_MACROS

// std
#include <list>
#include <algorithm>
#include <iterator>
#include <inttypes.h>

#ifdef _WIN32
#include "windows.h"
#endif

#include <QObject>
#include <QScopedPointer>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QByteArray>
#include <QLabel>
#include <QWidget>
#include <QString>
#include <QVector>
#include <QDebug>
#include <QMessageBox>
#include <QFile>
#include <QDir>
#include <QJsonObject>
#include <QThread>
//#include <QMutex>
//#include <QEventLoop>
#include <QListWidget>
#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(logDebug)
Q_DECLARE_LOGGING_CATEGORY(logInfo)
Q_DECLARE_LOGGING_CATEGORY(logWarning)
Q_DECLARE_LOGGING_CATEGORY(logCritical)

#define LOG_INFO(val)           qInfo(logInfo())<<val;
#define DEBUG_INFO(val)         qDebug(logDebug())<<val;
#define WARNING_INFO(val)       qWarning(logWarning())<<val;
#define CRITICAL_INFO(val)      qCritical(logCritical())<<val;


#endif // APPBACKEND_stdafx_h
